
Prerequisites
-------------
* npm
* Node 6

Instructions
-------------
* `cd nodejs_parser`
* `npm install`
* `node server.js`
* Visit `http://localhost:3000` to see a small test UI

Another way to test
--------------------
You can also test the logic by POSTing to the API
```
POST /api/v1/bankData
{"data":"my pasted bank data"}
```

The merged data will be returned, but you can also see it in `bankData.json`.