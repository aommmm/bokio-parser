'use strict';

const path = require('path');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const moment = require('moment');
const _ = require('lodash');

const PORT = 3000;
const DATA_PATH = './bankData.json';

// ----------------------------------------------------
// Init

// TODO use moment-timezone with swedish timezone instead,
// since presumably all given dates are in swedish time
// (Right now, it uses server's timezone when parsing)

const app = express();
app.use(bodyParser.json());

// ----------------------------------------------------
// Routes

app.get('/', get);
app.post('/api/v1/bankData', postBankData);
app.delete('/api/v1/bankData', delBankData);
app.get('/api/v1/testData', getTestData);

app.listen(PORT, function() {
    console.log('listenin on', PORT);
});

// ----------------------------------------------------
// Route implementations

function get(req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
}

function getTestData(req, res) {
    const testData = fs.readFileSync('./testData.json', 'utf8');
    res.send(testData);
}

function postBankData(req, res) {
    // Input validation
    let input = req.body.data;
    if (!input) {
        return res.status(401).send('data parameter not found');
    }

    // Parse input
    const newEvents = parseBankData(input);
    if (!newEvents.length) {
        return res.status(401).send('could not find the required information in the pasted data');
    }

    // Merge events with previously saved events
    const oldEvents = getBankData();
    const events = mergeBankData(oldEvents, newEvents);

    // Persist events and return them
    saveBankData(events);
    return res.status(200).send(events);
}

function delBankData(req, res) {
    try {
        fs.unlinkSync(DATA_PATH);
    } catch (e) {} // If it fails, it probably wasn't created in the first place. ignore

    res.status(200).send('ok');
}

// ----------------------------------------------------
// Parsing

class Event {
    /**
     * Creates a new event
     *
     * @param {String} date
     * @param {String} description
     * @param {Number} sum
     * @param {Number} total
     * @throws {Exception} if any fields are invalid
     */
    constructor(date, description, sum, total) {
        this.date = date;
        this.description = description;
        this.sum = sum;
        this.total = total;
        this.validate();
    }

    /**
     * Throws error if event is invalid
     */
    validate() {
        if (!this.date || this.date === 'Invalid date') {
            throw new Exception();
        }
        if (!this.description) {
            throw new Exception();
        }
        if (!_.isFinite(this.sum)) {
            throw new Exception();
        }
        if (!_.isFinite(this.total)) {
            throw new Exception();
        }
    }
}

/**
 * Try to parse string into array of events. On failure, returns empty array
 * @param {String} input
 * @returns {Event[]}
 */
function parseBankData(input) {
    const lines = input.split('\n');

    // Parsers to try
    let parsers = [parseSEBRow, parseHandelsbanken1Row, parseHandelsbanken2Row];

    // Try parsers until one of them gets results
    for (const parser of parsers) {
        const result = lines
                .map(parser)
                .filter(x => x);
        if (result.length) {
            return result;
        }
    }
    // No results
    return [];
}

/**
 * Parses String into Event for SEB
 * @param {String} row
 * @returns {Event|null} returns null if parsing failed
 */
function parseSEBRow(row) {
    const arr = row
        .split('\t')
        .filter(col => col !== 'Skicka'); // Remove potential 'Skicka' column
    try {
        const date0 = moment(arr[0], 'YYYY-MM-DD');
        const date1 = moment(arr[1], 'YYYY-MM-DD');
        const date = moment.min(date0, date1);
        return new Event(
            date.format('YYYY-MM-DD'),
            arr[3].trim(),
            parseNumber(arr[4].trim()),
            parseNumber(arr[5].trim())
        );
    } catch (e) {
        return null;
    }
}

/**
 * Parses String into Event for Handelsbanken (space-separated input)
 * @param {String} row
 * @returns {Event|null} returns null if parsing failed
 */
function parseHandelsbanken1Row(row) {
    const arr = row.split('   ');
    try {
        const date0 = moment(arr[0], 'YYYY-MM-DD');
        const date1 = moment(arr[1], 'YYYY-MM-DD');
        const date = moment.min(date0, date1);
        return new Event(
            date.format('YYYY-MM-DD'),
            arr[2].trim(),
            parseNumber(arr[3].trim()),
            parseNumber(arr[4].trim())
        );
    } catch (e) {
        return null;
    }
}

/**
 * Parses String into Event for Handelsbanken (tab-separated input)
 * @param {String} row
 * @returns {Event|null} returns null if parsing failed
 */
function parseHandelsbanken2Row(row) {
    const arr = row.split('\t \t');
    try {
        const date0 = moment(arr[0], 'YYYY-MM-DD');
        const date1 = moment(arr[1], 'YYYY-MM-DD');
        const date = moment.min(date0, date1);
        return new Event(
            date.format('YYYY-MM-DD'),
            arr[2].trim(),
            parseNumber(arr[3].trim()),
            parseNumber(arr[4].trim())
        );
    } catch (e) {
        return null;
    }
}

// ----------------------------------------------------
// DB actions

/**
 * Load data from disk
 * @returns {Object[]}
 */
function getBankData() {
    try {
        let bankData = fs.readFileSync(DATA_PATH, 'utf8');
        bankData = JSON.parse(bankData);
        return bankData;
    } catch (e) {
        return [];
    }
}

/**
 * Merges old and new events. We will not add new events again if they already exist
 * @param {Object[]} oldEvents
 * @param {Object[]} newEvents
 * @returns {Object[]} combined events
 */
function mergeBankData(oldEvents, newEvents) {
    // TODO document reasoning more
    let oldEventsStr = oldEvents.map(JSON.stringify);
    function oldEventExists(event) {
        // identical event exists?
        const i = oldEventsStr.findIndex(_.matches(JSON.stringify(event)));
        if (i === -1) {
            return false;
        } else {
            // remove from arr, to allow multiple occurrences
            oldEventsStr.splice(i, 1);
            return true;
        }
    }

    // If any new events already exist in oldEvents, do not add them
    newEvents = _.reject(newEvents, oldEventExists);

    let events = oldEvents.concat(newEvents);
    events = _.orderBy(events, 'date', 'desc');
    return events;
}

/**
 * Save data to disk
 * @param {Object[]} data
 */
function saveBankData(data) {
    data = JSON.stringify(data);
    fs.writeFileSync(DATA_PATH, data, 'utf8');
}

// ----------------------------------------------------
// Utils

/**
 *
 * @param {String} s
 * @returns {Number}
 */
function parseNumber(s) {
    // TODO: detect and handle also english number format ('1 000.00')
    return parseSwedishNumber(s);
}

/**
 *
 * @param {String} s
 * @returns {Number}
 */
function parseSwedishNumber(s) {
    s = s
        .replace(' ', '')
        .replace('.', '')
        .replace(',', '.');
    return Number(s);
}
